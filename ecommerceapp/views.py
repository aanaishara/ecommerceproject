from django.shortcuts import render

# Create your views here.
from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.urls import reverse_lazy, reverse
from django.views.generic import *
from .models import *
from .forms import *
# from django.contrib.messages.views import SuccessMessageMixin
# from django.conf import settings
# from django.core.mail import send_mail, send_mass_mail
# from urllib.parse import urlparse, parse_qs
# from django.utils import timezone
# from datetime import datetime, date, timedelta


class CustomerRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.groups.filter(name="customer").exists():
            pass
        else:
            return redirect("ecommerceapp:customersignin")
        return super().dispatch(request, *args, **kwargs)


class ClientBaseMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["root_categories"] = ProductCategory.objects.filter(root=None)
        cart_id = self.request.session.get("mycart", None)
        logged_in_user = self.request.user
        if logged_in_user.is_authenticated and logged_in_user.groups.filter(name="customer").exists():
            customer = Customer.objects.get(user=logged_in_user)
            if cart_id:
                cart_obj = Cart.objects.get(id=cart_id)
                cart_obj.customer = customer
                cart_obj.save()

        return context


class OrderCreateView(ClientBaseMixin, CustomerRequiredMixin, CreateView):
    template_name = "clienttemplate/ordercreate.html"
    form_class = OrderForm
    success_url = reverse_lazy("ecommerceapp:clienthome")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cart_id = self.request.session.get("mycart", None)
        cart = Cart.objects.get(id=cart_id)
        context["cart_obj"] = cart
        return context

    def form_valid(self, form):
        cart = Cart.objects.get(id=self.request.session.get("mycart"))
        form.instance.cart = cart
        form.instance.subtotal = cart.total
        form.instance.discount = 0
        form.instance.total = cart.total
        form.instance.order_status = "orderplaced"
        del self.request.session["mycart"]
        # cart_id=self.request.session.get("mycart")
        # del cart_id

        return super().form_valid(form)


class ClientHomeView(ClientBaseMixin, TemplateView):
    template_name = "clienttemplate/home.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["allproducts"] = Product.objects.all()

        return context

class CustomerProfileView(ClientBaseMixin, TemplateView):
    template_name = "clienttemplate/customerprofile.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
      
        context["order"] = Order.objects.all()

        return context
class ProfileDetailView(ClientBaseMixin, DetailView):
    template_name = "clienttemplate/profiledetail.html"
    model = Customer
    context_object_name = "customerobject"
# usersignup/singin


class CustomerSignupView(ClientBaseMixin, CreateView):
    template_name = "clienttemplate/customersignup.html"
    form_class = CustomerSignupForm
    success_url = reverse_lazy("ecommerceapp:clienthome")

    def form_valid(self, form):
        uname = form.cleaned_data["username"]
        email = form.cleaned_data["email"]
        password = form.cleaned_data["password"]
        user = User.objects.create_user(uname, email, password)
        form.instance.user = user

        return super().form_valid(form)


class CustomerSigninView(ClientBaseMixin, FormView):
    template_name = "clienttemplate/customersignin.html"
    form_class = SigninForm
    success_url = reverse_lazy("ecommerceapp:clienthome")

    def form_valid(self, form):
        uname = form.cleaned_data["username"]
        password = form.cleaned_data["password"]
        user = authenticate(username=uname, password=password)
        if user is not None:
            login(self.request, user)
        else:
            return render(self.request, self.template_name, {
                "error": "invalid credentials",
                "form": form,
                'root_categories': ProductCategory.objects.filter(root=None)
            })
        return super().form_valid(form)

    def get_success_url(self):
        logged_in_user = self.request.user
        if logged_in_user.groups.filter(name="customer").exists():
            return reverse("ecommerceapp:clienthome")
        else:
            return reverse("ecommerceapp:customersignup")


class CustomerSignoutView(View):
    def get(self, request):
        logout(request)
        return redirect("/")
    # to_manage_cart_item


class ManageCartView(ClientBaseMixin, TemplateView):
    template_name = "clienttemplate/managecart.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cartproduct_id = self.kwargs["pk"]
        action = self.kwargs["action"]
        cartproduct_obj = CartProduct.objects.get(id=cartproduct_id)
        cart = cartproduct_obj.cart
        if action == "inr":
            cartproduct_obj.quantity += 1
            cartproduct_obj.subtotal += cartproduct_obj.rate
            cartproduct_obj.save()
            cart.total += cartproduct_obj.rate
            cart.save()
            context["message"] = "product " + \
                cartproduct_obj.product.title + "increase succes"

        elif action == "dcr":
            cartproduct_obj.quantity -= 1
            cartproduct_obj.subtotal -= cartproduct_obj.rate
            cartproduct_obj.save()
            cart.total -= cartproduct_obj.rate
            cart.save()
            if cartproduct_obj.quantity == 0:
                cartproduct_obj.delete()
            context["message"] = "product" + \
                cartproduct_obj.product.title + "decrease succes"

        elif action == "rmv":
            product_title = cartproduct_obj.product.title
            cart.total -= cartproduct_obj.subtotal
            cart.save()
            cartproduct_obj.delete()
            context["message"] = product_title + "remove from cart"
        else:
            context["message"] = "invalid operation"

        return context


class EmptyCartView(ClientBaseMixin, TemplateView):
    template_name = "clienttemplate/managecart.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        c_id = self.request.session.get("mycart", None)
        if c_id:
            cart_obj = Cart.objects.get(id=c_id)
            cartproducts = cart_obj.cartproduct_set.all()
            cart_obj.total = 0
            cart_obj.save()
            cartproducts.delete()
            context["message"] = "cart is now empty"
        else:
            context["message"] = "you dont have item in cart"

        return context


class AdminHomeView(TemplateView):
    template_name = "admintemplate/home.html"


class CategoryDetailView(ClientBaseMixin, DetailView):
    template_name = "clienttemplate/categorydetail.html"
    model = ProductCategory
    context_object_name = "categoryobject"


class AddToCartView(ClientBaseMixin, TemplateView):
    template_name = "clienttemplate/addtocart.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        product_id = self.kwargs["pk"]
        product = Product.objects.get(id=product_id)
        cart_id = self.request.session.get("mycart", None)
        if cart_id:
            mycart = Cart.objects.get(id=cart_id)
            # print("old cart")
        else:
            mycart = Cart.objects.create(total=0)
            self.request.session["mycart"] = mycart.id
            # print("newcart")

        cartitemquery = mycart.cartproduct_set.filter(product=product)
        if cartitemquery.exists():
            cartprod = cartitemquery.first()
            cartprod.quantity += 1
            cartprod.subtotal += product.sp
            cartprod.save()
            mycart.total += product.sp
            mycart.save()
            # print("iteam already exits in cart")
        else:
            cartprod = CartProduct.objects.create(
                cart=mycart, product=product,
                rate=product.sp, quantity=1, subtotal=product.sp)
            mycart.total += product.sp
            mycart.save()

            # print("item doenot exit in cart")

        return context


class CartView(ClientBaseMixin, TemplateView):
    template_name = "clienttemplate/cart.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cart_id = self.request.session.get("mycart", None)
        try:
            mycart = Cart.objects.get(id=cart_id)
        except:
            mycart = Cart.objects.create(total=0)
            self.request.session["mycart"] = mycart.id
        context["mycart"] = mycart

        return context


class AdminCartListView(ListView):
    template_name = "admintemplate/admincartlist.html"
    queryset = Cart.objects.all().order_by('-id')
    context_object_name = "admincartlist"


class AdminOrderListView(ListView):
    template_name = "admintemplate/adminorderlist.html"
    queryset = Order.objects.all().order_by('-id')
    context_object_name = "adminorderlist"


class AdminCartListDetailView(DetailView):
    template_name = "admintemplate/admincartlistdetail.html"
    model = Cart
    context_object_name = "cartobject"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context


class AdminOrderListDetailView(DetailView):
    template_name = "admintemplate/adminorderlistdetail.html"
    model = Order
    context_object_name = "orderobject"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)

        return context

# admin product category


class AdminProductCategoryCreateView(CreateView):
    template_name = "admintemplate/adminproductcategorycreate.html"
    form_class = ProductCategoryForm
    success_url = reverse_lazy("ecommerceapp:adminproductcategorylist")


class AdminProductCategoryListView(ListView):
    template_name = "admintemplate/adminproductcategorylist.html"
    queryset = ProductCategory.objects.all().order_by('-id')
    context_object_name = "adminproductcategorylist"


class AdminProductCategoryDeleteView(DeleteView):
    template_name = "admintemplate/adminproductcategorydelete.html"
    model = ProductCategory
    success_url = reverse_lazy('ecommerceapp:adminproductcategorylist')


class AdminProductCategoryUpdateView(UpdateView):
    template_name = "admintemplate/adminproductcategorycreate.html"
    form_class = ProductCategoryForm
    model = ProductCategory
    success_url = reverse_lazy('ecommerceapp:adminproductcategorylist')

    # admin product


class AdminProductCreateView(CreateView):
    template_name = "admintemplate/adminproductcreate.html"
    form_class = ProductForm
    success_url = reverse_lazy("ecommerceapp:adminproductlist")


class AdminProductListView(ListView):
    template_name = "admintemplate/adminproductlist.html"
    queryset = Product.objects.all().order_by('-id')
    context_object_name = "adminproductlist"


class AdminProductDeleteView(DeleteView):
    template_name = "admintemplate/adminproductdelete.html"
    model = Product
    success_url = reverse_lazy('ecommerceapp:adminproductlist')


class AdminProductUpdateView(UpdateView):
    template_name = "admintemplate/adminproductcreate.html"
    form_class = ProductForm
    model = Product
    success_url = reverse_lazy('ecommerceapp:adminproductlist')

    # admin product image


class AdminProductImageCreateView(CreateView):
    template_name = "admintemplate/adminproductimagecreate.html"
    form_class = ProductImageForm
    success_url = reverse_lazy("ecommerceapp:adminproductimagelist")


class AdminProductImageListView(ListView):
    template_name = "admintemplate/adminproductimagelist.html"
    queryset = ProductImage.objects.all().order_by('-id')
    context_object_name = "adminproductimagelist"


class AdminProductImageDeleteView(DeleteView):
    template_name = "admintemplate/adminproductimagedelete.html"
    model = ProductImage
    success_url = reverse_lazy('ecommerceapp:adminproductimagelist')


class AdminProductImageUpdateView(UpdateView):
    template_name = "admintemplate/adminproductimagecreate.html"
    form_class = ProductImageForm
    model = ProductImage
    success_url = reverse_lazy('ecommerceapp:adminproductimagelist')
