from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register([Admin,Customer,CartProduct,Cart,ProductImage,Product,ProductCategory,Order])
