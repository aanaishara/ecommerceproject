from django import forms
from .models import *
# from django.contrib.auth.models import User


class CustomerSignupForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput())
    email = forms.EmailField(widget=forms.EmailInput())
    password = forms.CharField(widget=forms.PasswordInput())
    confirm_password = forms.CharField(widget=forms.PasswordInput())

    class Meta:
        model = Customer
        fields = ["username", "email", "password",
                  "confirm_password", "name", "mobile", "address", "photo"]


class SigninForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput())
    password = forms.CharField(widget=forms.PasswordInput())


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ["municipality", "street_address",
                  "alt_mobile", "delivery_date", "payment_mode"]


class ProductCategoryForm(forms.ModelForm):
    class Meta:
        model = ProductCategory
        fields = ["title", "slug", "image", "root"]


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ["title", "sku", "category", "mrp", "sp"]

class ProductImageForm(forms.ModelForm):
    class Meta:
        model = ProductImage
        fields = ["product", "image"]
