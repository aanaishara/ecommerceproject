from django.contrib.auth.models import Group
from django.contrib.auth.models import User
from django.db import models
from .constants import *

# Create your models here.


class TimeStamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)
    active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Admin(TimeStamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    mobile = models.CharField(max_length=50)
    address = models.CharField(max_length=200)
    photo = models.ImageField(upload_to="admin")

    def __str__(self):
        return self.name


class Customer(TimeStamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    mobile = models.CharField(max_length=50)
    address = models.CharField(max_length=200, null=True, blank=True)
    photo = models.ImageField(upload_to="admin", null=True, blank=True)

    def save(self, *args, **kwargs):
        grp, created = Group.objects.get_or_create(name="customer")
        self.user.groups.add(grp)

        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class ProductCategory(TimeStamp):
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique=True)
    image = models.ImageField(upload_to="category")
    root = models.ForeignKey('self',
                             on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.title


class Product(TimeStamp):
    title = models.CharField(max_length=200)
    sku = models.CharField(max_length=200, unique=True)
    category = models.ForeignKey(ProductCategory, on_delete=models.CASCADE)
    mrp = models.DecimalField(max_digits=19, decimal_places=2)
    sp = models.DecimalField(max_digits=19, decimal_places=2)


# property method

    def image(self):
        return self.productimage_set.first().image

    def __str__(self):
        return self.title


class ProductImage(TimeStamp):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    image = models.ImageField(upload_to="prodcutimage")

    def __str__(self):
        return "(" + self.product.title + ")"


class Cart(TimeStamp):
    customer = models.ForeignKey(
        Customer, on_delete=models.CASCADE, null=True, blank=True)
    total = models.DecimalField(max_digits=19, decimal_places=2)

    def __str__(self):
        return str(self.id)


class CartProduct(TimeStamp):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    rate = models.DecimalField(max_digits=19, decimal_places=2)
    quantity = models.PositiveIntegerField()
    subtotal = models.DecimalField(max_digits=19, decimal_places=2)

    def __str__(self):
        return "(" + self.product.title + ")"


class Order(TimeStamp):
    cart = models.OneToOneField(Cart, on_delete=models.CASCADE)
    municipality = models.CharField(max_length=200, null=True, blank=True)
    street_address = models.CharField(max_length=200, null=True, blank=True)
    alt_mobile = models.CharField(max_length=200, null=True, blank=True)
    subtotal = models.DecimalField(max_digits=19, decimal_places=2)
    discount = models.DecimalField(max_digits=19, decimal_places=2)
    total = models.DecimalField(max_digits=19, decimal_places=2)
    delivery_date = models.DateField(null=True, blank=True)
    order_status = models.CharField(max_length=50, choices=ORDER_STATUS)
    payment_mode = models.CharField(max_length=50, choices=PAYMENT_METHOD)

    def __str__(self):
        return "orderid: " + str(self.id)
