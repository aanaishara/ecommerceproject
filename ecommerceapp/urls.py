from django.urls import path
from .views import *
# from .views import


app_name = "ecommerceapp"

urlpatterns = [
    path('', ClientHomeView.as_view(), name='clienthome'),
    path('category/<slug:slug>', CategoryDetailView.as_view(), name="categorydetail"),
    path('add-to-cart/<int:pk>/', AddToCartView.as_view(), name="addtocart"),
    path('cart/', CartView.as_view(), name="cart"),
    path('manage-cart/<int:pk>/<action>/',
         ManageCartView.as_view(), name="managecart"),
    path('empty-cart/', EmptyCartView.as_view(), name="emptycart"),
    path('customer/sign-up', CustomerSignupView.as_view(), name="customersignup"),
    path('customer/sign-in', CustomerSigninView.as_view(), name="customersignin"),
    path("customer/sign-out", CustomerSignoutView.as_view(), name="customersignout"),
    path("customer/profile", CustomerProfileView.as_view(), name="customerprofile"),
    path('customer/<int:pk>/profile',
         ProfileDetailView.as_view(), name='profiledetail'),

    path('order/', OrderCreateView.as_view(), name="ordercreate"),




    path('med-admin/home/', AdminHomeView.as_view(), name='adminhome'),
    path('med-admin/cartlist', AdminCartListView.as_view(), name='admincartlist'),

    path("med-admin/cartlist/<int:pk>/detail/",
         AdminCartListDetailView.as_view(), name="admincartlistdetail"),
    path('med-admin/orderlist', AdminOrderListView.as_view(), name='adminorderlist'),
    path("med-admin/orderlist/<int:pk>/detail/",
         AdminOrderListDetailView.as_view(), name="adminorderlistdetail"),

    # admin product category
    path('med-admin/productcategory/create',
         AdminProductCategoryCreateView.as_view(), name='adminproductcategorycreate'),
    path('med-admin/productcategory/list',
         AdminProductCategoryListView.as_view(), name='adminproductcategorylist'),
    path('med-admin/productcategory/<int:pk>/delete',
         AdminProductCategoryDeleteView.as_view(), name='adminproductcategorydelete'),
    path('med-admin/productcategory/<int:pk>/update',
         AdminProductCategoryUpdateView.as_view(), name='adminproductcategoryupdate'),

    # admin product
    path('med-admin/product/create',
         AdminProductCreateView.as_view(), name='adminproductcreate'),
    path('med-admin/product/list',
         AdminProductListView.as_view(), name='adminproductlist'),
    path('med-admin/product/<int:pk>/delete',
         AdminProductDeleteView.as_view(), name='adminproductdelete'),
    path('med-admin/product/<int:pk>/update',
         AdminProductUpdateView.as_view(), name='adminproductupdate'),

    # admin product image
    path('med-admin/productimage/create',
         AdminProductImageCreateView.as_view(), name='adminproductimagecreate'),
    path('med-admin/productimage/list',
         AdminProductImageListView.as_view(), name='adminproductimagelist'),
    path('med-admin/productimage/<int:pk>/delete',
         AdminProductImageDeleteView.as_view(), name='adminproductimagedelete'),
    path('med-admin/productimage/<int:pk>/update',
         AdminProductImageUpdateView.as_view(), name='adminproductimageupdate'),




]
